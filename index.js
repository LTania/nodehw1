const express = require('express');
const bodyParser = require('body-parser')
const morgan = require('morgan');
const cors = require('cors');

const { createFile, getFiles, getFile} = require('./controllers/fileController')
const { checkFileTypes } = require('./middlewares/checkFileType')

const PORT = 8080;
const app = express();

app.use(express.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(morgan('tiny'));
app.use(cors());

app.post('/api/files', checkFileTypes, createFile);
app.post('/api/api/files', checkFileTypes, createFile);
app.get('/api/files', getFiles);
app.get('/api/api/files', getFiles);
app.get('/api/files/:filename', getFile);
app.get('/api/api/files/:filename', getFile);

app.listen(PORT, () =>{
    console.log(`Server has been started on ${PORT}...`)
});
