const path = require('path');

const ALLOWEDEXT = ['.log', '.txt', '.json', '.yaml', '.xml', '.js'];

module.exports.checkFileTypes = (req, res, next) => {
    const { filename, content } = req.body;
    const ext = path.extname(filename);
    const isAllowed = ALLOWEDEXT.includes(ext);

    if(!isAllowed){
        return res.status(400).json({ message:`File extension is not allowed` });
    }

    if(!content){
        return res.status(400).json({ message:`Please specify 'content' parameter` });
    }

    next();
}
