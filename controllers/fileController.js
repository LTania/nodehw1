const fs = require('fs');
const path = require('path');

module.exports.createFile = (req, res) => {
    const { filename, content } = req.body;
    if (!fs.existsSync('./myFiles')){
        fs.mkdirSync('./myFiles');
    }
    fs.writeFile(`./myFiles/${filename}`, content, (err) => {
        if(err){
            return res.status(500).json({ message:"Server error" });
        } else {
            return res.status(200).json({ message:"File created successfully" });
        }
    });
}

module.exports.getFiles = (req, res) => {
    try {
        if (!fs.existsSync('./myFiles')){
            fs.mkdirSync('./myFiles');
        }
        const files = fs.readdirSync('./myFiles/');

        if(!files.length){
            return res.status(400).json({ message: `Client error` });
        }
        return res.status(200).json({
            message: "Success",
            files: files
        });
    } catch (e) {
        return res.status(500).json({ message: `Server error` });
    }
}

module.exports.getFile = (req, res) => {
        const { filename } = req.params;
        if (!fs.existsSync('./myFiles')){
            fs.mkdirSync('./myFiles');
        }

        if(!fs.existsSync(`./myFiles/${filename}`)){
            return res.status(400).json({ message: `No file with '${filename}' filename found` });
        }

        try {
            const content = fs.readFileSync(`./myFiles/${filename}`, 'utf8');

            return res.status(200).json({
                message: 'Success',
                filename,
                content,
                extension: path.extname(filename).slice(1),
                uploadedData: fs.statSync(`./myFiles/${filename}`).birthtime,
            });

        } catch (err) {
            return res.status(500).json({ message: `Server error` });
        }
}
